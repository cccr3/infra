output "database_endpoint" {
  value = module.aurora.rds_cluster_endpoint
}