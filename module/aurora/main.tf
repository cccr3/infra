terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.62.0"
    }
  }
}

provider "aws" {
  region = local.region
}

locals {
  name   = "mysql"
  region = "ap-northeast-2"
  tags = {
    Owner       = "user"
    Environment = "dev"
  }
}

resource "random_password" "master" {
  length = 10
}

module "aurora" {
  source = "terraform-aws-modules/rds-aurora/aws"

  name                  = local.name
  engine_mode           = "multimaster"
  instance_type         = "db.r4.2xlarge"

  vpc_id                = "vpc-095e32e2f82783a47"
  db_subnet_group_name  = "eks-vpc"
  create_security_group = true
  allowed_cidr_blocks   = ["10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24"]

  replica_count                       = 2
  iam_database_authentication_enabled = true
  password                            = random_password.master.result
  create_random_password              = false

  apply_immediately   = true
  skip_final_snapshot = true

  db_parameter_group_name         = aws_db_parameter_group.example.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.example.id
  #enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]

  tags = local.tags
}

resource "aws_db_parameter_group" "example" {
  name        = "${local.name}-aurora-db-56-parameter-group"
  family      = "aurora5.6"
  description = "${local.name}-aurora-db-56-parameter-group"
  tags        = local.tags
}

resource "aws_rds_cluster_parameter_group" "example" {
  name        = "${local.name}-aurora-56-cluster-parameter-group"
  family      = "aurora5.6"
  description = "${local.name}-aurora-56-cluster-parameter-group"
  tags        = local.tags

  parameter {
    name = "time_zone"
    value = "Asia/Seoul"
  }

  parameter {
		name = "character_set_server"
		value = "utf8mb4"
	}

	parameter {
		name = "character_set_client"
		value = "utf8mb4"
	}

	parameter {
		name = "character_set_connection"
		value = "utf8mb4"
	}

	parameter {
		name = "character_set_database"
		value = "utf8mb4"
	}

	parameter {
		name = "character_set_filesystem"
		value = "utf8mb4"
	}

	parameter {
		name = "character_set_results"
		value = "utf8mb4"
	}

	parameter {
		name = "collation_connection"
		value = "utf8mb4_general_ci"
	}

	parameter {
		name = "collation_server"
		value = "utf8mb4_general_ci"
	}
}


########################################################################################################################
# S3
########################################################################################################################
