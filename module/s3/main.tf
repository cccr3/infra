terraform {
  backend "s3" {
    region         = "ap-northeast-2"
    bucket         = "cccr3-tfstate-test-you-can-delete-after-1025"
    key            = "s3.tfstate"
    dynamodb_table = "terraform-resource-lock"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.62.0"
    }

    null = {
      source = "hashicorp/null"
      version = "3.1.0"
    }

  }
}

provider "aws" {
  # Configuration options
  region  = "ap-northeast-2"
  profile = "default"
}

provider "null" {
  # Configuration options
}

resource "aws_s3_bucket" "front" {
  bucket = "cccr3-frontend-test-you-can-delete-after-1025"
  acl    = "public-read"

  website {
    index_document = "index.html"
  }
  tags = {
    ENV = "Dev"
  }
}

//resource "null_resource" "upload_to_s3" {
//  provisioner "local-exec" {
//    command = "aws s3 sync dist s3://${aws_s3_bucket.front.id} --acl public-read"
//  }
//}


//resource "aws_s3_bucket_object" "front" {
//  bucket = aws_s3_bucket.front.id
//  key = "test"
//  source = "./emoji.png"
//}

//resource "aws_s3_bucket_object" "count_test" {
//  count  = length(var.files)
//  bucket = aws_s3_bucket.front.id
//  acl    = "public-read"
//  key    = var.files[count.index]
//  source = var.files[count.index]
//}
//
//variable "files" {
//  type    = list(string)
//  default = ["index.html", "favicon.ico", "css", "js", "usertradeicon.ico"]
//}
