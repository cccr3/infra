output "s3_bucket_name" {
  value = aws_s3_bucket.front.id
}

output "s3_bucket_arn" {
  value = aws_s3_bucket.front.arn
}

output "s3_bucket_domain_name" {
  value = aws_s3_bucket.front.bucket_domain_name
}

output "s3_website_endpoint" {
  value = aws_s3_bucket.front.website_endpoint
}

output "s3_website_domain" {
  value = aws_s3_bucket.front.website_domain
}

output "s3_url" {
  value = aws_s3_bucket.front.bucket_regional_domain_name
}