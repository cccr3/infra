variable "tfstate_s3_bucket_name" {
  description = "you have to create this s3 bucket before terraform init"
  type    = string
  default = "cccr3-tfstate-test-you-can-delete-after-1025"
}
