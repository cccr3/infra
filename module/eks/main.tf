terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.60.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.5.0"
    }
  }
}

data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

provider "aws" {
  # Configuration options
  region  = "ap-northeast-2"
  profile = "default"
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.20.0"
  # insert the 7 required variables here

  cluster_name    = "cccr3"
  cluster_version = "1.20"
  subnets         = ["subnet-016ae50ef9b3488f8", "subnet-0e5fb1eb3d17dadd6", "subnet-0a089e366f0ecb4f5"]
  vpc_id          = "vpc-095e32e2f82783a47"

  map_users = [
    {
      userarn  = "arn:aws:iam::371156277055:user/jadenpark"
      username = "jadenpark"
      groups   = ["system:masters"]

    },
    {
      userarn  = "arn:aws:iam::371156277055:user/rlatkddyd987"
      username = "rlatkddyd987"
      groups   = ["system:masters"]

    },
    {
      userarn  = "arn:aws:iam::371156277055:user/mzmz01"
      username = "mzmz01"
      groups   = ["system:masters"]

    }
  ]
  map_roles = [
    {
      rolearn = "arn:aws:iam::371156277055:role/eks-test"
      username = "cloud9"
      groups = ["system:masters"]
    }
  ]

  node_groups = {
    example = {
      name_prefix      = "cccr3_node_group"
      desired_capacity = 3
      max_capacity     = 10
      min_capacity     = 1

      instance_types = ["t3.small"]

      additional_tags = {
        env   = "dev",
        owner = "cccr3"
      }
    }
  }

}
