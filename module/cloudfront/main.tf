terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.60.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "ap-northeast-2"
  profile = "default"
}

resource "aws_cloudfront_distribution" "CDN" {
  comment = "cccr3-front CDN"

  origin {
    domain_name = "cccr3-usertrade-front-app.s3.ap-northeast-2.amazonaws.com"
    origin_id = "cccr3-usertrade-front-app"
  }
  enabled = true

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = "cccr3-usertrade-front-app"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }

  price_class = "PriceClass_200"


  default_root_object = "index.html"

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}