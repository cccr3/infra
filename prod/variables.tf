##########  Network ##########
variable "vpc_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "private_subnets" {
  type = list(string)
}

variable "database_subnets" {
  type = list(string)
}

variable "tags" {
  type = map(string)
}
########### EKS ##########
variable "cluster_name_prefix" {
  type = string
}

variable "cluster_version" {
  type = string
}

variable "node_group_name" {
  type = string
}

variable "node_instance_type" {
  type = list(string)
}

variable "node_desired_capacity" {
  type = number
}

variable "node_max_capacity" {
  type = number
}

variable "node_min_capacity" {
  type = number
}

variable "node_group_tags" {
  type = map(string)
}

##########  DB  ##########
variable "engine_mode" {
  type = string
}

variable "db_instance_type" {
  type = string
}

variable "db_allowed_cidr_blocks" {
  type = list(string)
}

variable "db_replica_count" {
  type = number
}

variable "db_pg_name" {
  type = string
}

variable "pg_family" {
  type = string
}

